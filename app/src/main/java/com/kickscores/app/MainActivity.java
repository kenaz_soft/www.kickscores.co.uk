package com.kickscores.app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class MainActivity extends Activity {

    public final static String TAG = "MainActivity";

    private WebView webview;
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionbar = getActionBar();
        if( actionbar!= null){
            actionbar.hide();
        }
//        ActionBar actionbar = getActionBar();
//        if(actionbar !=null) {
//            actionbar.setTitle(R.string.app_name);
//            actionbar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
//        }
        webview = (WebView)findViewById(R.id.webview);

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        webview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d(TAG, "Processing webview url click...");
                if (url.startsWith("http://www.dailymotion.com") || url.startsWith("https://twitter.com")) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                    return true;
                }
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Log.d(TAG, "Started loading URL: " + url);
                try {
                    progressBar = ProgressDialog.show(MainActivity.this, null, "Loading...");
                } catch (Throwable t) {

                }
            }

            public void onPageFinished(WebView view, String url) {
                Log.d(TAG, "Finished loading URL: " + url);
                if (progressBar != null && progressBar.isShowing()) {
                    try {
                        progressBar.dismiss();
                    } catch (Throwable t) {

                    }
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);

            }
        });
        String siteUrl = getResources().getString(R.string.site_url);
        webview.loadUrl(siteUrl);
    }
}
